abstract class SignalingClientType {
  static const agent = 'agent';
  static const customer = 'customer';
}

abstract class SignalingClientEventType {
  static const join = 'join';
  static const offer = 'offer';
  static const answer = 'answer';
  static const icecandidate = 'icecandidate';
}

abstract class SignalingServerEventType {
  static const offer = 'offer';
  static const answer = 'answer';
  static const icecandidate = 'icecandidate';
}
