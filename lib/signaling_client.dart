import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:socket_io_client/socket_io_client.dart';

import 'constant.dart';

class SignalingClientService {
  final IO.Socket socket;

  SignalingClientService(String uri)
      : this.socket = IO.io(
          uri,
          OptionBuilder().setTransports(['websocket']).enableForceNew().build(),
        );

  void _log(String message) => debugPrint('[$runtimeType] $message');

  void _emitEvent(String eventType, Object payload) =>
      socket?.emit(eventType, jsonEncode(payload));

  void _onEvent(String eventType, void Function(Object) onEventData) =>
      socket?.on(eventType, (data) => onEventData(jsonDecode(data)));

  void join(String clientType, String name) => socket?.onConnect((_) {
        _log('connected');
        _emitEvent(
          SignalingClientEventType.join,
          {'clientType': clientType, 'name': name},
        );
      });

  void offer(Object offer) => _emitEvent(SignalingClientEventType.offer, offer);

  void answer(Object answer) =>
      _emitEvent(SignalingClientEventType.answer, answer);

  void icecandidate(Object icecandidate) =>
      _emitEvent(SignalingClientEventType.icecandidate, icecandidate);

  void onOffer(void Function(Object) onEventData) =>
      _onEvent(SignalingClientEventType.offer, onEventData);

  void onAnswer(void Function(Object) onEventData) =>
      _onEvent(SignalingClientEventType.answer, onEventData);

  void onIcecandidate(void Function(Object) onEventData) =>
      _onEvent(SignalingClientEventType.icecandidate, onEventData);

  void dispose() => socket?.dispose();
}
