import 'dart:html';

import 'package:custom_web_rtc/constant.dart';
import 'package:custom_web_rtc/signaling_client.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webrtc/webrtc.dart';

class VideoCall extends StatefulWidget {
  final String clientType, name;

  const VideoCall({Key key, @required this.clientType, @required this.name})
      : super(key: key);

  @override
  _VideoCallState createState() => _VideoCallState();
}

class _VideoCallState extends State<VideoCall> {
  RTCVideoRenderer _localRenderer, _remoteRenderer;
  SignalingClientService _signalingClientService;
  RTCPeerConnection _rtcPeerConnection;

  MediaStream _localStream, _remoteStream;

  MediaRecorder _mediaRecorder;

  bool get _isCustomer => widget.clientType == SignalingClientType.customer;

  bool _onCall;
  bool _recording;

  final Map<String, dynamic> mineDefaultSdpConstraints = {
    "mandatory": {
      "OfferToReceiveAudio": true,
      "OfferToReceiveVideo": true,
      'iceRestart': true
    },
    "optional": [],
  };

  @override
  void initState() {
    super.initState();
    _onCall = false;
    _recording = false;
    _localRenderer = RTCVideoRenderer();
    _remoteRenderer = RTCVideoRenderer();

    final Map<String, dynamic> mediaConstraints = {
      'audio': false,
      'video': {
        'facingMode': 'user',
      },
    };

    Future.wait([
      Future.value(_localRenderer.initialize()),
      Future.value(_remoteRenderer.initialize()),
    ]).then(
      (_) => navigator.getUserMedia(mediaConstraints).then(
        (stream) async {
          _localRenderer.srcObject = stream;
          _localStream = stream;
          _rtcPeerConnection = await createPeerConnection(
            {
              'iceServers': [
                {'url': 'stun:stun.l.google.com:19302'},
                {
                  "url": "turn:35.184.112.236:3478",
                  "username": "username",
                  "credential": "password",
                },
              ]
            },
            {},
          );
          _rtcPeerConnection.addStream(stream);
          _rtcPeerConnection.onAddStream = (stream) {
            print(stream);
            _remoteStream = stream;
            _remoteRenderer.srcObject = stream;
          };
          print('starting');
          _signalingClientService =
              // SignalingClientService('http://f0eefbab39f5.ngrok.io')
              SignalingClientService('http://35.184.112.236:8080')
                ..join(widget.clientType, widget.name)
                ..onIcecandidate(
                  (icecandidate) {
                    print(icecandidate);
                    if (icecandidate is Map) {
                      _rtcPeerConnection.addCandidate(
                        RTCIceCandidate(
                          icecandidate['candidate'],
                          icecandidate['sdpMid'],
                          icecandidate['sdpMlineIndex'],
                        ),
                      );
                    }
                  },
                )
                ..onOffer(
                  (offer) {
                    print(offer);
                    if (offer is Map) {
                      _rtcPeerConnection.setRemoteDescription(
                        RTCSessionDescription(offer['sdp'], offer['type']),
                      );
                      showDialog(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                          title: Text("Customer offer"),
                          content: Text("answer the call?"),
                          actions: <Widget>[
                            FlatButton(
                              child: Text("answer"),
                              onPressed: () async {
                                Navigator.of(context).pop();
                                _onCall = true;
                                setState(() {});
                                final answer = await _rtcPeerConnection
                                    .createAnswer(mineDefaultSdpConstraints);
                                final payload = {
                                  'sdp': answer.sdp,
                                  'type': answer.type
                                };
                                _rtcPeerConnection.setLocalDescription(answer);
                                _signalingClientService.answer(payload);
                              },
                            ),
                          ],
                        ),
                      );
                    }
                  },
                )
                ..onAnswer(
                  (answer) {
                    print(answer);
                    if (answer is Map) {
                      _rtcPeerConnection.setRemoteDescription(
                        RTCSessionDescription(answer['sdp'], answer['type']),
                      );
                    }
                  },
                );
          _rtcPeerConnection.onIceConnectionState = print;
          _rtcPeerConnection.onIceGatheringState = print;
          _rtcPeerConnection.onIceCandidate = (candidate) {
            if (candidate == null) {
              print('onIceCandidate: complete!');
              return;
            }
            final payload = {
              'sdpMLineIndex': candidate.sdpMlineIndex,
              'sdpMid': candidate.sdpMid,
              'candidate': candidate.candidate,
            };
            print('Getting new ice candidate -> $payload');
            _signalingClientService.icecandidate(payload);
          };
        },
      ),
    );
  }

  @override
  void dispose() {
    _localRenderer?.dispose();
    _remoteRenderer?.dispose();
    _signalingClientService?.dispose();
    _rtcPeerConnection?.close();
    _localStream?.dispose();
    _remoteStream?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: Text('Video Call')),
        body: Center(
          child: Column(
            children: [
              OrientationBuilder(
                builder: (context, orientation) => _onCall
                    ? Container(
                        width: MediaQuery.of(context).size.width * 2 / 3,
                        height: MediaQuery.of(context).size.height * 2 / 3,
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              left: 0.0,
                              right: 0.0,
                              top: 0.0,
                              bottom: 0.0,
                              child: Container(
                                margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                width:
                                    MediaQuery.of(context).size.width * 2 / 3,
                                height:
                                    MediaQuery.of(context).size.height * 2 / 3,
                                child: RTCVideoView(_remoteRenderer),
                                decoration:
                                    BoxDecoration(color: Colors.black54),
                              ),
                            ),
                            Positioned(
                              left: 20.0,
                              top: 20.0,
                              child: Container(
                                width: orientation == Orientation.portrait
                                    ? 90.0
                                    : 120.0,
                                height: orientation == Orientation.portrait
                                    ? 120.0
                                    : 90.0,
                                child: RTCVideoView(_localRenderer),
                                decoration:
                                    BoxDecoration(color: Colors.black54),
                              ),
                            ),
                          ],
                        ),
                      )
                    : Container(
                        margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                        width: MediaQuery.of(context).size.width * 2 / 3,
                        height: MediaQuery.of(context).size.height * 2 / 3,
                        child: RTCVideoView(_localRenderer),
                        decoration: BoxDecoration(color: Colors.black54),
                      ),
              ),
              if (!_onCall && _isCustomer)
                RaisedButton(
                  child: Text('offer'),
                  onPressed: () async {
                    _onCall = true;
                    setState(() {});
                    final offer = await _rtcPeerConnection
                        .createOffer(mineDefaultSdpConstraints);
                    _rtcPeerConnection.setLocalDescription(offer);

                    final payload = {'sdp': offer.sdp, 'type': offer.type};
                    _signalingClientService.offer(payload);
                  },
                ),
              if (_onCall && !_isCustomer)
                RaisedButton(
                  child: Text(_recording ? 'stop record' : 'record'),
                  onPressed: () async {
                    if (!_recording) {
                      print('recording?');
                      _recording = true;
                      setState(() {});
                      _mediaRecorder = MediaRecorder()
                        ..startWeb(
                          _remoteStream,
                          onDataChunk: (blob, islastOne) {
                            final url = Url.createObjectUrlFromBlob(blob);
                            print(url);
                            final anchorElement = AnchorElement(href: url);
                            anchorElement.download = url;
                            anchorElement.click();
                          },
                        );
                      return;
                    }
                    print('stop recording?');
                    _recording = false;
                    setState(() {});
                    _mediaRecorder?.stop();
                  },
                ),
            ],
          ),
        ),
      );
}
