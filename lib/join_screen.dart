import 'package:flutter/material.dart';

import 'constant.dart';
import 'video_call.dart';

class JoinScreen extends StatefulWidget {
  @override
  _JoinScreenState createState() => _JoinScreenState();
}

class _JoinScreenState extends State<JoinScreen> {
  TextEditingController _nameController;
  String _clientType;

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
    _clientType = SignalingClientType.customer;
  }

  @override
  void dispose() {
    _nameController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: Text('Join Screen')),
        body: Center(
          child: SizedBox(
            width: 300,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextField(
                  controller: _nameController,
                  decoration: InputDecoration(labelText: 'Name'),
                ),
                RadioListTile<String>(
                  title: const Text('Customer'),
                  value: SignalingClientType.customer,
                  groupValue: _clientType,
                  onChanged: (value) => setState(() => _clientType = value),
                ),
                RadioListTile<String>(
                  title: const Text('Agent'),
                  value: SignalingClientType.agent,
                  groupValue: _clientType,
                  onChanged: (value) => setState(() => _clientType = value),
                ),
                RaisedButton(
                  child: Text('Join'),
                  onPressed: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => VideoCall(
                        clientType: _clientType,
                        name: _nameController.text ?? 'anonymous',
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
